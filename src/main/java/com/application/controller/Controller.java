package com.application.controller;

import io.swagger.annotations.ApiParam;
import org.bson.Document;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.application.services.EtudiantService;

import java.util.List;
import java.util.Map;

@RestController()
@RequestMapping("/etudiants")
public class Controller {

    private final EtudiantService etudiantService;

    public Controller(EtudiantService etudiantService) {
        this.etudiantService = etudiantService;
    }

    @GetMapping()
    public ResponseEntity<List<Document>> getAllStudent(@ApiParam(hidden = true) @RequestParam(required = false) Map<String,String> allParams) {
        List<Document> documents = etudiantService.getAll(allParams);

        return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON).body(documents);
    }

    @GetMapping("{idBooster}")
    public ResponseEntity<Document> getEtudiantParIdBooster(@PathVariable int idBooster) {
        Document document = etudiantService.getId(idBooster);

        return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON).body(document);
    }

    @GetMapping("/best")
    public ResponseEntity<List<Document>> MeilleursEtudiant(@ApiParam(hidden = true) @RequestParam(required = false) Map<String,String> allParams) {
        List<Document> bestStudents = etudiantService.get10Best(allParams);

        return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON).body(bestStudents);
    }
    
    @GetMapping("/abandon")
    public ResponseEntity<List<Document>> EtudiantAbandon(@ApiParam(hidden = true) @RequestParam(required = false) Map<String,String> allParams) {
        List<Document> abandonE = etudiantService.getAbandon(allParams);

        return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON).body(abandonE);
    }

    @GetMapping("/campus")
    public ResponseEntity<List<Document>> getNbrEtudiantParCampus(@ApiParam(hidden = true) @RequestParam(required = false) Map<String,String> allParams){
        List<Document> studentsForEachCampus = etudiantService.countEtudiantParCampus(allParams);

        return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON).body(studentsForEachCampus);
    }

    @GetMapping("/enterprises")
    public ResponseEntity<List<Document>> getNbrEtudiantEnApprentissage() {
        List<Document> documents = etudiantService.CountApprentiesEntreprise();

        return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON).body(documents);
    }
    
    @GetMapping("/FairStudent")
    public ResponseEntity<List<Document>> ApprentiesSalon() {
        List<Document> documents = etudiantService.ApprentiesFairStudent();

        return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON).body(documents);
    }

    @GetMapping("/enterprises/{enterprises}")
    public ResponseEntity<List<Document>> getEtudiantParEntreprise(@PathVariable String enterprises) {
        List<Document> documents = etudiantService.getEtudiantParEntreprise(enterprises);

        return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON).body(documents);
    }

    
    @GetMapping("/embauche")
    public ResponseEntity<List<Document>> getEmbauche() {
        List<Document> documents = etudiantService.getPromessEmbaucheParEntreprise();

        return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON).body(documents);
    }
    @GetMapping("/note/moyen/annee")
    public ResponseEntity<List<Document>> getMoyenNoteParAn() {
        List<Document> documents = etudiantService.getMoyenGlobalParAn();

        return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON).body(documents);
    }

    @GetMapping("/note/moyen/campus")
    public ResponseEntity<List<Document>> getMoyenNoteParCampus() {
        List<Document> documents = etudiantService.getMoyenGlobalParCampus();

        return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON).body(documents);
    }

    @GetMapping("/note/moyen/enterprises")
    public ResponseEntity<List<Document>> getMoyenNoteParEntreprise() {
        List<Document> documents = etudiantService.getMoyenGlobalParEntreprise();

        return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON).body(documents);
    }

    @GetMapping("/ects/moyen")
    public ResponseEntity<List<Document>> getECTSMoyenParAn(@ApiParam(hidden = true) @RequestParam(required = false) Map<String,String> allParams) {
        List<Document> documents = etudiantService.getMoyenGlobalECTSParAn(allParams);

        return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON).body(documents);
    }

    @GetMapping("/ects/ratio")
    public ResponseEntity<List<Document>> getECTSRatioParAn(@ApiParam(hidden = true) @RequestParam(required = false) Map<String,String> allParams) {
        List<Document> documents = etudiantService.getRatioECTSGlobalParAns(allParams);

        return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON).body(documents);
    }

}
