package com.application.configuration;

import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.SparkSession;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Config {


    @Bean
    public JavaSparkContext javaSparkContext(){
        SparkSession sparkSession = SparkSession.builder()
                .appName("5DATA")
                .master("local[2]")
                .config("spark.mongodb.input.uri", "mongodb://localhost/5data.Students")
                .config("spark.mongodb.output.uri", "mongodb://localhost/5data.Students")
                .getOrCreate();
        return new JavaSparkContext(sparkSession.sparkContext());
    }
}
