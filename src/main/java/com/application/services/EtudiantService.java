package com.application.services;

import com.jayway.jsonpath.JsonPath;
import com.mongodb.spark.MongoSpark;
import com.mongodb.spark.rdd.api.java.JavaMongoRDD;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.bson.Document;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import scala.Tuple2;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static java.util.Collections.singletonList;

@Service
public class EtudiantService {


    private final JavaSparkContext javaSparkContext;

    public EtudiantService(JavaSparkContext javaSparkContext) {
        this.javaSparkContext = javaSparkContext;
    }

    public List<Document> getAll(Map<String, String> params) {
        JavaMongoRDD<Document> data = loadData();

        List<Document> pipeline = createPipelineQuery(params);

        JavaMongoRDD<Document> donnees = data.withPipeline(pipeline);
        return donnees.collect();
    }

    public Document getId(int idBooster) {
        JavaMongoRDD<Document> data = loadData();

        List<Document> pipeline = singletonList(
                Document.parse("{ $match: { \"IDBooster\": " + idBooster + " } }")
        );

        JavaMongoRDD<Document> donnees = data.withPipeline(pipeline);
        return donnees.first();

    }
    
    
   

    public List<Document> get10Best(Map<String, String> params) {
        JavaMongoRDD<Document> data = loadData();

        List<Document> pipeline = createPipelineQuery(params);
        pipeline.add(Document.parse("{ $sort: { Mean: -1 } }"));

        JavaMongoRDD<Document> donnees = data.withPipeline(pipeline);
        return donnees.take(10);
    }
    
    public List<Document> getAbandon(Map<String, String> params) {
        JavaMongoRDD<Document> data = loadData();

        List<Document> pipeline = createPipelineQuery(params);
          pipeline.add(Document.parse("{ $match: { \"DropOut\": 1 } }")
        );

        JavaMongoRDD<Document> donnees = data.withPipeline(pipeline);
        return donnees.take(10);

    }


    public List<Document> countEtudiantParCampus(Map<String, String> params) {
        JavaMongoRDD<Document> data = loadData();

        List<Document> pipeline = createPipelineQuery(params);
        JavaMongoRDD<Document> donnees = data.withPipeline(pipeline);

        JavaPairRDD<String, Integer> EtudiantParCampus = donnees.mapToPair(document ->
                new Tuple2<>(JsonPath.read(document.toJson(), "$.CampusCity"), 1)
        );

        Map<String, Long> CarteEtudiantParCampus = EtudiantParCampus.countByKey();

        List<Document> documents = new java.util.ArrayList<>(Collections.emptyList());

        for (Map.Entry<String, Long> stringIntegerEntry : CarteEtudiantParCampus.entrySet()) {
            documents.add(Document.parse(String.format("{'%s':%s}", stringIntegerEntry.getKey(), stringIntegerEntry.getValue())));
        }
        return documents;
    }

    public List<Document> CountApprentiesEntreprise() {
        JavaMongoRDD<Document> data = loadData();

        List<Document> pipeline = Arrays.asList(
                Document.parse("{ $match: { \"Apprenticeship\": 1 } }"),
                Document.parse("{ $sortByCount: \"$Enterprise\" }")
        );

        JavaMongoRDD<Document> donnees = data.withPipeline(pipeline);
        return donnees.take(10);

    }
    
    public List<Document> ApprentiesFairStudent() {
        JavaMongoRDD<Document> data = loadData();

        List<Document> pipeline = Arrays.asList(
                //Document.parse("{ $match: { \"Apprenticeship\": 1 } }"),
                Document.parse("{ $match: { \"FairStudent\": 1 } }"),
                Document.parse("{ $sortByCount: \"$CampusCity\" }")
        );

        JavaMongoRDD<Document> donnees = data.withPipeline(pipeline);
        return donnees.take(20);

    } // a revoir
    
    public List<Document> getPromessEmbaucheParEntreprise() {
        JavaMongoRDD<Document> data = loadData();

        List<Document> pipeline = Arrays.asList(
                Document.parse("{ $match: { \"JobOffer\": 1 } }"),
                Document.parse("{ $sortByCount: \"$Enterprise\" }")
        );

        JavaMongoRDD<Document> donnees = data.withPipeline(pipeline);
        return donnees.take(10);

    }
    
    

    public List<Document> getEtudiantParEntreprise(String enterprise) {
        JavaMongoRDD<Document> data = loadData();

        List<Document> pipeline = Arrays.asList(
                Document.parse("{ $match: { \"Apprenticeship\": 1 } }"),
                Document.parse("{ $match: { \"Enterprise\": '" + enterprise + "' } }")
        );

        JavaMongoRDD<Document> donnees = data.withPipeline(pipeline);
        return donnees.collect();
    }

//    public List<Document> getPromessEmbaucheParEntreprise(String enterprise) {
//        JavaMongoRDD<Document> data = loadData();
//
//        List<Document> pipeline = Arrays.asList(
//                Document.parse("{ $match: { \"JobOffer\": 1 } }"),
//                Document.parse("{ $match: { \"Enterprise\": '" + enterprise + "' } }")
//        );
//
//        JavaMongoRDD<Document> donnees = data.withPipeline(pipeline);
//        return donnees.collect();
//    }

    
    public List<Document> getMoyenGlobalParCampus() {
        JavaMongoRDD<Document> data = loadData();


        JavaPairRDD<String, Integer> flatStudentsMean = data.mapToPair(document ->
                new Tuple2<>(
                        JsonPath.read(document.toJson(), "$.CampusCity"),
                        JsonPath.read(document.toJson(), "$.Mean")
                )
        );

        JavaPairRDD<String, Tuple2<Integer, Double>> test = flatStudentsMean.mapValues(value -> new Tuple2<>(value, 1D));
        JavaPairRDD<String, Tuple2<Integer, Double>> test2 = test.reduceByKey((a, b) -> new Tuple2<>(a._1 + b._1, a._2 + b._2));
        JavaPairRDD<String, Double> test3 = test2.mapValues(value -> value._1 / value._2);
        Map<String, Double> stringIntegerMap = test3.collectAsMap();

        List<Document> documents = new java.util.ArrayList<>(Collections.emptyList());

        for (Map.Entry<String, Double> stringIntegerEntry : stringIntegerMap.entrySet()) {
            documents.add(Document.parse(String.format("{'%s':%s}", stringIntegerEntry.getKey(), stringIntegerEntry.getValue())));
        }
        return documents;
    }

    public List<Document> getMoyenGlobalParAn() {
        JavaMongoRDD<Document> data = loadData();


        JavaPairRDD<String, Integer> MoyenEtudiant = data.mapToPair(document ->
                new Tuple2<>(
                        JsonPath.read(document.toJson(), "$.StudyYear"),
                        JsonPath.read(document.toJson(), "$.Mean")
                )
        );

        JavaPairRDD<String, Tuple2<Integer, Double>> test = MoyenEtudiant.mapValues(value -> new Tuple2<>(value, 1D));
        JavaPairRDD<String, Tuple2<Integer, Double>> test2 = test.reduceByKey((a, b) -> new Tuple2<>(a._1 + b._1, a._2 + b._2));
        JavaPairRDD<String, Double> test3 = test2.mapValues(value -> value._1 / value._2);
        Map<String, Double> stringIntegerMap = test3.collectAsMap();

        List<Document> documents = new java.util.ArrayList<>(Collections.emptyList());

        for (Map.Entry<String, Double> stringIntegerEntry : stringIntegerMap.entrySet()) {
            documents.add(Document.parse(String.format("{'Year %s':%s}", stringIntegerEntry.getKey(), stringIntegerEntry.getValue())));
        }
        return documents;
    }

    public List<Document> getMoyenGlobalParEntreprise() {
        JavaMongoRDD<Document> data = loadData();


        JavaPairRDD<String, Integer> flatStudentsMean = data.mapToPair(document ->
                new Tuple2<>(
                        JsonPath.read(document.toJson(), "$.Enterprise"),
                        JsonPath.read(document.toJson(), "$.Mean")
                )
        );

        JavaPairRDD<String, Tuple2<Integer, Double>> test = flatStudentsMean.mapValues(value -> new Tuple2<>(value, 1D));
        JavaPairRDD<String, Tuple2<Integer, Double>> test2 = test.reduceByKey((a, b) -> new Tuple2<>(a._1 + b._1, a._2 + b._2));
        JavaPairRDD<String, Double> test3 = test2.mapValues(value -> value._1 / value._2);
        Map<String, Double> stringIntegerMap = test3.collectAsMap();

        List<Document> documents = new java.util.ArrayList<>(Collections.emptyList());

        for (Map.Entry<String, Double> stringIntegerEntry : stringIntegerMap.entrySet()) {
            documents.add(Document.parse(String.format("{'%s':%s}", stringIntegerEntry.getKey(), stringIntegerEntry.getValue())));
        }
        return documents;
    }

    public List<Document> getMoyenGlobalECTSParAn(Map<String, String> params) {
        JavaMongoRDD<Document> data = loadData();

        List<Document> pipeline = createPipelineQuery(params);
        JavaMongoRDD<Document> donnees = data.withPipeline(pipeline);

        JavaPairRDD<String, Integer> MoyenEtudiant = donnees.mapToPair(document ->
                new Tuple2<>(
                        JsonPath.read(document.toJson(), "$.StudyYear"),
                        JsonPath.read(document.toJson(), "$.ECTSCredits")
                )
        );

        JavaPairRDD<String, Tuple2<Integer, Double>> test = MoyenEtudiant.mapValues(value -> new Tuple2<>(value, 1D));
        JavaPairRDD<String, Tuple2<Integer, Double>> test2 = test.reduceByKey((a, b) -> new Tuple2<>(a._1 + b._1, a._2 + b._2));
        JavaPairRDD<String, Double> test3 = test2.mapValues(value -> value._1 / value._2);
        Map<String, Double> stringIntegerMap = test3.collectAsMap();

        List<Document> documents = new java.util.ArrayList<>(Collections.emptyList());

        for (Map.Entry<String, Double> stringIntegerEntry : stringIntegerMap.entrySet()) {
            documents.add(Document.parse(String.format("{'Year %s':%s}", stringIntegerEntry.getKey(), stringIntegerEntry.getValue())));
        }


        return documents;

    }

    public List<Document> getRatioECTSGlobalParAns(Map<String, String> params) {
        JavaMongoRDD<Document> data = loadData();

        List<Document> pipeline = createPipelineQuery(params);
        JavaMongoRDD<Document> donnees = data.withPipeline(pipeline);

        JavaPairRDD<Double, Document> MoyenEtudiant = donnees.mapToPair(document -> new Tuple2<>(0D, document));

        JavaRDD<Tuple2<Double, Document>> map = MoyenEtudiant.map(value -> new Tuple2<Double, Document>(
                Math.floor((Double.parseDouble(value._2.get("ECTSCredits").toString()) / (60 * Double.parseDouble(value._2.get("StudyYear").toString()))) * 100D),
                value._2
        ));

        List<Document> documents = new java.util.ArrayList<>(Collections.emptyList());

        List<Tuple2<Double, Document>> list = map.collect();

        for (Tuple2<Double, Document> stringIntegerEntry : list) {
            documents.add(Document.parse(String.format("{'score': %s, 'student': %s}", stringIntegerEntry._1(), stringIntegerEntry._2().toJson())));
        }

        return documents;

    }

    @Cacheable("data")
    public JavaMongoRDD<Document> loadData() {
        return MongoSpark.load(javaSparkContext);
    }

    private List<Document> createPipelineQuery(Map<String, String> params) { // faire un if sur 
        List<Document> pipeline = new java.util.ArrayList<>(Collections.emptyList());

        for (Map.Entry<String, String> entry : params.entrySet()) {
            Document document = Document.parse("{ $match: { \"" + entry.getKey() + "\": " + entry.getValue() + " } }");
            pipeline.add(document);
        }

        return pipeline;
    }
}
